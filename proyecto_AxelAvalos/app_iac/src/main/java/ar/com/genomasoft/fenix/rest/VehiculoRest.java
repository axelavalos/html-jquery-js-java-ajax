package ar.com.genomasoft.fenix.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import ar.com.genomasoft.fenix.model.Persona;
import ar.com.genomasoft.fenix.model.Vehiculo;
import ar.com.genomasoft.fenix.reports.PdfGeneratorUtil;
import ar.com.genomasoft.fenix.service.PersonaService;
import ar.com.genomasoft.fenix.service.VehiculoService;
import ar.com.genomasoft.jproject.core.daos.ConditionEntry;
import ar.com.genomasoft.jproject.core.daos.ConditionSimple;
import ar.com.genomasoft.jproject.core.daos.SearchOption;
import ar.com.genomasoft.jproject.core.exception.BaseException;
import ar.com.genomasoft.jproject.webutils.webservices.BaseClientAuditedEntityWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api("Vehiculos - Servicio web REST")
@RequestMapping(path = "/api/vehiculo")
public class VehiculoRest extends BaseClientAuditedEntityWebService<Vehiculo, VehiculoService> {

	@Autowired
	PdfGeneratorUtil pdfGenaratorUtil;

	@Autowired
	VehiculoService vservice;

	@GetMapping(path = "/listar")
	@ApiOperation(value = "Listas de vehiculos")
	public @ResponseBody Collection<Vehiculo> getListVehiculosSinAfiliar() throws Exception {
		return this.service.findAll();
	}

	@GetMapping(path = "/pdf", produces = { MediaType.APPLICATION_PDF_VALUE })
	@ApiOperation(value = "Listado de vehiculos.")
	public @ResponseBody byte[] getPdf() throws Exception {
		Map<Object, Object> data = new HashMap<Object, Object>();
		data.put("titulo", "Vehiculos");
		data.put("vehiculos", service.findAll());
		byte[] reporte = pdfGenaratorUtil.createPdf("/pdf/vehiculos", data);
		return reporte;
	}

	@GetMapping(path = "/validar/{vehiculo}")
	public @ResponseBody String validar(@PathVariable("vehiculo") String vehiculo) throws Exception {

		Vehiculo V = new Gson().fromJson(vehiculo, Vehiculo.class);

		List<ConditionEntry> condiciones = new ArrayList<ConditionEntry>();

		condiciones.add(new ConditionSimple("marca", SearchOption.EQUAL, V.getMarca()));
		condiciones.add(new ConditionSimple("modelo", SearchOption.EQUAL, V.getModelo()));
		condiciones.add(new ConditionSimple("anio", SearchOption.EQUAL, V.getAnio()));
		condiciones.add(new ConditionSimple("color", SearchOption.EQUAL, V.getColor()));

		Collection<Vehiculo> vehiculos = vservice.findByFilters(condiciones);

		if (vehiculos.size() > 0) {
			System.out.println("usuario no disponible");

		} else {
			V.setMarca("");
			V.setCreatedTime(new Date());
			V.setCreatedByUser(1);

			vservice.save(V);
			System.out.println("usuario creado correctamente");
		}
		return vehiculo;

	}
	
 }
