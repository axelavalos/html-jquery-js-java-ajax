package ar.com.genomasoft.fenix;




import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;



import ar.com.genomasoft.fenix.config.Application;
import ar.com.genomasoft.fenix.model.Persona;
import ar.com.genomasoft.fenix.service.PersonaService;
import ar.com.genomasoft.jproject.core.exception.BaseException;

@SpringBootTest
@RunWith(SpringRunner.class)
@TransactionConfiguration(defaultRollback=true)
@Transactional
@WebAppConfiguration
@ContextConfiguration(classes = {Application.class})
public class ApplicationTests {

	@Autowired
	PersonaService pservice;

	ArrayList<Persona> listaPersona = new ArrayList<Persona>();

	
	@Test
	public void contextLoads() throws BaseException {
			Persona pers1 = new Persona();
			pers1.setNombre("Lucas");
			pers1.setEdad(23);
			
			listaPersona.add(pers1);
			
			Persona pers2 = new Persona();
			pers2.setNombre("David");
			pers2.setEdad(55);
			
			listaPersona.add(pers2);
			
			Persona pers3 = new Persona();
			pers3.setNombre("Luna");
			pers3.setEdad(65);
			
			listaPersona.add(pers3);
			
			Persona pers4 = new Persona();
			pers4.setNombre("Casey");
			pers4.setEdad(14);

			listaPersona.add(pers4);
			
			Persona pers5 = new Persona();
			pers5.setNombre("Sam");
			pers5.setEdad(29);
			
			listaPersona.add(pers5);
			
			
//			pers = pservice.save(pers);
			System.out.println("Guardo");			
	}

	@Test
	public void recorrer() throws BaseException {
for (Persona persona : listaPersona) {
	
}	
	
	
}
}